---
editor_options: 
  markdown: 
    wrap: 72
---

## Foreword

This is just an update to aknowledge that we have a publication on GROAN. 
There is no change in the code from previous 1.3.0 release.

## Test environments

-   local R installation, ubuntu 20.04, R 4.0.3
-   win-builder (devel)

## R CMD check results

0 errors \| 0 warnings \| 1 note

**NOTE 1 - NELSON SAYS**: checked by hand, both are working (I suspect bot protection)

<pre>
> On windows-x86_64-devel (r-devel), ubuntu-gcc-release (r-release), fedora-clang-devel (r-devel)
* checking CRAN incoming feasibility ... [39s] NOTE
Maintainer: 'Nelson Nazzicari <nelson.nazzicari@gmail.com>'

Found the following (possibly) invalid URLs:
  URL: https://www.jstor.org/stable/25681325
    From: man/GROAN.AI.Rd
          man/GROAN.KI.Rd
    Status: 403
    Message: Forbidden
</pre>